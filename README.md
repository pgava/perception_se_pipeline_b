## The perception_se_pipeline_b package

This package contains the code the process lidar and radar pointclouds and outputs a fused sensor grid map with map grid map.

An example node can be found in src/src/example_node.cpp .