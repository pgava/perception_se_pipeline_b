#include<iostream>
#include"record_processor.hpp"
#include<sensor_msgs/PointCloud2.h>
#include<thread>
#include<variant>
#include"record_single_thread_pool.hpp"
#include"pipeline_b_processor.hpp"
#include"rodoa_type_traits.hpp"
#include<chrono>

using rodoa_pc_record = single_info_record<sensor_msgs::PointCloud2>;
using rodoa_record_list_type = std::variant<rodoa_pc_record, int, sensor_msgs::PointCloud2>;
using rodoa_record_processor = pipeline_b_processor;

template<>
auto rodoa_record_processor::process_input<sensor_msgs::PointCloud2 &>(sensor_msgs::PointCloud2& record){
  std::cout << "Processing sensor_msgs::PointCloud2 record" << std::endl;
}

using rodoa_thread_pool = record_single_thread_pool<rodoa_record_processor, rodoa_record_list_type>;

int main(int argc, char** argv)
{

    std::cout << type_in_pack<rodoa_pc_record, rodoa_record_list_type>::value << std::endl;

    auto tf_buffer = std::make_shared<tf2_ros::Buffer>();

    rodoa_thread_pool tp(std::make_shared<rodoa_record_processor>(tf_buffer, 0.5));
    tp.start();
    auto pc = sensor_msgs::PointCloud2();
    for(std::size_t i=0; i<10; ++i){

        auto pc = sensor_msgs::PointCloud2();
        rodoa_pc_record record{{"some_topic"}, pc};
        
        tp.enqueue(pc);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

    }

    std::cout << "Test passed" << std::endl;
    return 0;
}