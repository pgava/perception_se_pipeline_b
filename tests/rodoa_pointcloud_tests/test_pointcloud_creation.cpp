#include<iostream>
#include"rodoa_pointcloud.hpp"

int main(int argc, char** argv)
{
    auto l_pc_ptr = rodoa_pointcloud_factory::create_pointcloud<lidar_pc_t>();
    auto r_pc_ptr = rodoa_pointcloud_factory::create_pointcloud<radar_pc_t>();
    
    if(r_pc_ptr->get_field<std::vector<float>>("doppler").has_value())
        if(r_pc_ptr->get_field<std::vector<float>>("doppler").value()->empty()){
            std::cout << "Test passed" << std::endl;
        }

    // if(!l_pc_ptr->get_field<std::vector<float>>("doppler").has_value())
    //     std::cout << "Test passed" << std::endl;
    
    return 0;
}