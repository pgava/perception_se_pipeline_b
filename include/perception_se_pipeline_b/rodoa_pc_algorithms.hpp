#ifndef RODOA_PC_ALGORITHMS_HPP
#define RODOA_PC_ALGORITHMS_HPP

#include<memory>
#include"rodoa_pointcloud.hpp"
#include<Eigen/Dense>

class rodoa_pc_algorithms{
  
  static void transform_doppler_values_(radar_pc_ptr& rpc, Eigen::Matrix4d &tf);

public:
  template<typename PointCloudType>
  static std::unique_ptr<PointCloudType> &transform_rpc(std::unique_ptr<PointCloudType> &rpc, Eigen::Matrix4d &tf);

  static radar_pc_ptr &remove_points_by_conf(radar_pc_ptr &rpc, float threshold);
  
  template<typename PointCloudType>
  static std::unique_ptr<PointCloudType> &filter_pc_by_z_height(std::unique_ptr<PointCloudType> &rpc, float z_min, float z_max);
  
  template<typename PointCloudType>
  static std::unique_ptr<PointCloudType> &filter_pc_by_z_height(std::unique_ptr<PointCloudType> &rpc, float z_min, float z_max);
  
  
  
  static radar_pc_ptr & radar_compensate_doppler(radar_pc_ptr &rpc, Eigen::Matrix4d &vf2rf,
                                                 Eigen::Vector3d &linear_vel, Eigen::Vector3d &angular_vel,
                                                 double confidence_threshold = 0.4,
                                                 bool abs_doppler = true,
                                                 bool inverted_vel = true);

};



void rodoa_pc_algorithms::transform_doppler_values_(radar_pc_ptr& rpc, Eigen::Matrix4d &tf){
//...
}

radar_pc_ptr &rodoa_pc_algorithms::remove_points_by_conf(radar_pc_ptr& rpc, float threshold){
//...
}

template<typename PointCloudType>
std::unique_ptr<PointCloudType> &filter_pc_by_z_height(unique_ptr<PointCloudType> &rpc, float z_min, float z_max){
    
  // Find the indices of points with z coordinate outside the interval [z_min, z_max]
  std::vector<int> indices;
  indices.reserve(rpc->pc.points_.size());
    
  for (std::size_t i = 0; i < rpc->pc.points_.size(); ++i) {
    if (points[i][2] < z_min || points[i][2] > z_max) {
      indices.push_back(i);
    }
  }
    
  bool has_colors = rpc->pc.colors_.size() == rpc->pc.points_.size();
  bool has_normals = rpc->pc.normals_.size() == rpc->pc.points_.size();

  // Remove the points with z coordinate outside the interval [z_min, z_max]
  rpc->pc.points_ = remove_elements_by_index(rpc->pc.points_, indices);
  if (has_colors) {
    remove_elements_by_index(rpc.pc.colors_, indices);
  }
  if (has_normals) {
    remove_elements_by_index(rpc->pc.normals_, indices);
  }

  if constexpr(std::is_same<unique_ptr<PointCloudType>, radar_pc_ptr>){
    bool has_doppler = rpc->get_field<vector<float>>("doppler").size() == rpc->pc.points_.size();
    if (has_doppler) {
      vector<float> &doppler_ref_vec = rpc->get_field<vector<float>>("doppler");
      remove_elements_by_index(doppler_ref_vec, indices);
    }
  }

  return rpc;
}

template<typename PointCloudType>
unique_ptr<PointCloudType> &rodoa_pc_algorithms::transform_rpc(unique_ptr<PointCloudType> &rpc, Eigen::Matrix4d &tf){
  rpc.pc.Transform(tf);
  if constexpr(std::is_same<unique_ptr<PointCloudType>, radar_pc_ptr>){
    transform_doppler_values_(rpc, tf);
  }
}

#endif // RODOA_PC_ALGORITHMS_HPP