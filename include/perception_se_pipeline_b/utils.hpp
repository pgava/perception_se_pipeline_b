#ifndef UTILS_HPP
#define UTILS_HPP

#include<vector>
#include<type_traits>

template<typename T>
void remove_elements_by_index(std::vector<T>& vec,
                              const std::vector<int>& indices){

  int j = 0;

  for (auto it = vec.begin(); it != vec.end(); it++) {
    if (j < indices.size() && std::distance(vec.begin(), it) == indices[j]) {
      j++;
    } else {
      if (j > 0) {
        *(it - j) = std::move(*it);
      }
    }
  }
  vec.resize(vec.size() - indices.size());
  
}

#endif // UTILS_HPP