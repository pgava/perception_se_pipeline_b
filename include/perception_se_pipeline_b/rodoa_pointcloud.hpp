#ifndef RODOA_POINTCLOUD_HPP
#define RODOA_POINTCLOUD_HPP

#include<memory>
#include<variant>
#include<map>
#include<optional>
#include<type_traits>
#include<string>
#include<vector>
#include<iostream>
#include<sensor_msgs/PointCloud2.h>
#include<functional>

#include"rodoa_type_traits.hpp"

struct rodoa_pointcloud_factory;

template <typename... ExtraFieldsTypes>
class rodoa_pointcloud{
  // open3d::geometry::PointCloud pc;
  
  struct badge;
  
public:

  explicit rodoa_pointcloud(rodoa_pointcloud::badge, rodoa_pointcloud &rpc) : rodoa_pointcloud(std::move(rpc)) {}

  template<typename T, std::enable_if_t<type_in_pack<T, ExtraFieldsTypes...>::value, int> = 0>
  auto get_field(std::string& field_name){

    if constexpr (sizeof...(ExtraFieldsTypes) > 0){
      if(std::holds_alternative<T>(extra_fields_[field_name]))
        return std::make_optional<T *>(&(std::get<T>(extra_fields_[field_name])));
    }
    
    return std::optional<T *>{};

  }
  
  template<typename T, std::enable_if_t<type_in_pack<T, ExtraFieldsTypes...>::value, int> = 0>
  auto get_field(const std::string&& field_name){

    if constexpr (sizeof...(ExtraFieldsTypes) > 0){
      if(std::holds_alternative<T>(extra_fields_[field_name]))
        return std::make_optional<T *>(&(std::get<T>(extra_fields_[field_name])));
    }
    
    return std::optional<T *>{};

  }
  
private:

  rodoa_pointcloud(){};

  rodoa_pointcloud(rodoa_pointcloud &&rpc){
    if constexpr(sizeof...(ExtraFieldsTypes) > 0){
      std::swap(extra_fields_, rpc.extra_fields_);
    }
  };

  struct empty_type : std::false_type{};
  
  struct badge{};
    
  friend rodoa_pointcloud_factory;

  std::map<std::string, std::variant<empty_type, ExtraFieldsTypes...>> extra_fields_;

};

using lidar_pc_t = rodoa_pointcloud<>;
using radar_pc_t = rodoa_pointcloud<std::vector<float>>;

using lidar_pc_ptr = std::unique_ptr<lidar_pc_t>;
using radar_pc_ptr = std::unique_ptr<radar_pc_t>;

struct rodoa_pointcloud_factory{

  // rodoa_pointcloud_factory get_instance(){ return rodoa_pointcloud_factory{}; }

  template<typename PointCloudType>
  static std::unique_ptr<PointCloudType> ros_pc2_to_rodoa_pc(sensor_msgs::PointCloud2::ConstPtr pc2){
    
    auto pc = PointCloudType{};

    if constexpr(std::is_same_v<PointCloudType, lidar_pc_t>){
      
    }
    else if constexpr(std::is_same_v<PointCloudType, radar_pc_t>){
      // pc.extra_fields_["doppler"] = std::vector<float>{};
    }
  
    return std::make_unique<PointCloudType>(typename PointCloudType::badge(), pc);

  }

  template<typename PointCloudType>
  static std::unique_ptr<PointCloudType> create_pointcloud(){
  
    auto pc = PointCloudType{};

    if constexpr(std::is_same_v<PointCloudType, lidar_pc_t>){
      
    }
    else if constexpr(std::is_same_v<PointCloudType, radar_pc_t>){
      pc.extra_fields_["doppler"] = std::vector<float>{};
    }
  
    return std::make_unique<PointCloudType>(typename PointCloudType::badge(), pc);
  
  }
  
  
};

#endif  // RODOA_POINTCLOUD_HPP