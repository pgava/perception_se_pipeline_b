#ifndef RODOA_TYPE_TRAITS_HPP
#define RODOA_TYPE_TRAITS_HPP

#include <type_traits>
#include <variant>
#include <memory>

template<typename T, typename ... Args>
struct type_in_pack {
    static constexpr bool value {(std::is_same_v<T, Args> || ...)};
};

template<typename T, typename ... Args>
struct type_in_pack<T, std::variant<Args...>>{
    static constexpr bool value {(std::is_same_v<T, Args> || ...)};
};

#endif  // RODOA_TYPE_TRAITS_HPP