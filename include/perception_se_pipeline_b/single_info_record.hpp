#ifndef SINGLE_INFO_RECORD_HPP
#define SINGLE_INFO_RECORD_HPP

#include<memory>
#include<string>
#include<optional>

template<typename T>
struct single_info_record{

    std::optional<std::string> label;
    T value;

};

#endif  // SINGLE_INFO_RECORD_HPP