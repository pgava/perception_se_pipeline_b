#ifndef RODOA_RECORD_SINGLE_THREAD_POOL_HPP
#define RODOA_RECORD_SINGLE_THREAD_POOL_HPP

#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <variant>
#include <memory>

#include"rodoa_type_traits.hpp"

template<typename... SingleRecordTypes>
using record_shrd_ptr_list_t = std::variant<std::shared_ptr<SingleRecordTypes>...>;

template<typename... SingleRecordTypes>
using record_unq_ptr_list_t = std::variant<std::unique_ptr<SingleRecordTypes>...>;

template<typename InputProcessor, typename InputListType>
class record_single_thread_pool {
public:

  record_single_thread_pool(std::shared_ptr<InputProcessor> processor) : processor_(processor), stop_(false) {}

  template<typename R, std::enable_if_t<type_in_pack<std::remove_const_t<std::remove_reference_t<R>>, InputListType>::value, int> = 0>
  void enqueue(R&& record) {
    std::unique_lock<std::mutex> lock(queue_mutex_);
    input_queue_.push(record);
    lock.unlock();
    condition_.notify_one();
  }

  void start() {

    bool &stop = stop_;
    auto &queue_mutex = queue_mutex_;
    auto &condition = condition_;
    auto &input_queue = input_queue_;
    auto &processor = processor_;

    worker_thread_ = std::thread([&stop, &queue_mutex, &condition, &input_queue, &processor] {
      while (true) {
        std::unique_lock<std::mutex> lock(queue_mutex);
        condition.wait(lock, [&stop, &input_queue] { return !input_queue.empty() || stop; });
        if (stop && input_queue.empty()) {
          return;
        }
        auto record = std::move(input_queue.front());
        input_queue.pop();
        lock.unlock();
        std::visit([&processor](auto&& arg) { processor->process_input(arg); }, record);
      }
    });
  }

  void stop() {
    {
      std::unique_lock<std::mutex> lock(queue_mutex_);
      stop_ = true;
    }
    condition_.notify_all();
    worker_thread_.join();
  }

  ~record_single_thread_pool() {
    stop();
  }

  const std::queue<InputListType> &get_queue() const {
    return input_queue_;
  }

private:
  std::thread worker_thread_;
  std::queue<InputListType> input_queue_;
  std::mutex queue_mutex_;
  std::condition_variable condition_;
  bool stop_;
  std::shared_ptr<InputProcessor> processor_;
};

#endif // RODOA_RECORD_SINGLE_THREAD_POOL_HPP