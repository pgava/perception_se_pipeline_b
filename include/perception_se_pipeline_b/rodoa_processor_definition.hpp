#ifndef RODOA_PROCESSOR_DEFINITION_HPP
#define RODOA_PROCESSOR_DEFINITION_HPP

#include "pipeline_b_processor.hpp"
#include "record_single_thread_pool.hpp"

//This is the definition of which and how many types of msgs we want to process===========================================
using rodoa_record_list_type = std::variant<sensor_msgs::PointCloud2>;
using rodoa_input_processor = pipeline_b_processor;

template<>
auto rodoa_input_processor::process_input<sensor_msgs::PointCloud2 &>(sensor_msgs::PointCloud2 & record){
  std::cout << "Processing sensor_msgs::PointCloud2ConstPtr & msg" << std::endl;
}

using rodoa_thread_pool = record_single_thread_pool<rodoa_input_processor, rodoa_record_list_type>;
//========================================================================================================================

#endif  // RODOA_PROCESSOR_DEFINITION_HPP