#ifndef PIPELINE_B_PROCESSOR_HPP
#define PIPELINE_B_PROCESSOR_HPP

//local
#include"single_info_record.hpp"
#include"rodoa_pointcloud.hpp"

//c++17
#include<optional>
#include<memory>
#include<variant>
#include<type_traits>
#include<map>

//Eigen
#include<Eigen/Dense>

//ros
#include<sensor_msgs/PointCloud2.h>
#include<tf2_ros/buffer.h>
#include<grid_map_msgs/GridMap.h>

class pipeline_b_processor {
public:
    
    pipeline_b_processor(std::shared_ptr<tf2_ros::Buffer> tf_buffer_ptr, float grid_resolution){};
    ~pipeline_b_processor(){};  

    template<typename T>
    auto process_input(T&& data) {}

    void load_site_map(const std::string& file_name) {
        std::cout << "Loading site map from " << file_name << std::endl;
    }

    const std::shared_ptr<grid_map_msgs::GridMap> get_grid_map() const {
        std::cout << "Getting grid map" << std::endl;
        return grid_map_to_send_;
    }

private:

    grid_map_msgs::GridMap grid_map_model_;
    std::vector<Eigen::Vector2d> changed_cells_;
    std::shared_ptr<grid_map_msgs::GridMap> grid_map_to_send_;

    std::shared_ptr<tf2_ros::Buffer> tf_buffer_ptr_;
    
    std::map<std::string, lidar_pc_ptr> lidar_readings_;
    std::map<std::string, std::size_t> lidar_readings_stamps_ns_;
    std::map<std::string, radar_pc_ptr> radar_readings_;
    std::map<std::string, std::size_t> radar_readings_stamps_ns_;


};

#endif // PIPELINE_B_PROCESSOR_HPP

