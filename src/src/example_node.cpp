#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf2_ros/transform_listener.h>

#include"rodoa_processor_definition.hpp" // <-- The only include needed for the lib to work

class PointCloudNode
{
public:
  PointCloudNode(std::string&& site_map_path, float grid_resolution) : nh_("~") {
    // Set up the subscriber to the input point cloud topic
    cloud_sub_ = nh_.subscribe("cloud_topic", 1, &PointCloudNode::cloudCallback, this);

    // Set up the publisher for the output point cloud topic
    cloud_pub_ = nh_.advertise<sensor_msgs::PointCloud2>("cloud_topic", 1);

    grid_pub_ = nh_.advertise<grid_map_msgs::GridMap>("grid_map_topic", 1);

    //This is our lib setup================================================================================================

    // Set up the tf buffer
    tf_buffer_ = std::make_shared<tf2_ros::Buffer>();
    tf_buffer_->setUsingDedicatedThread(true);

    // Set up the subscriber for the tf transform
    tf_listener_ = std::make_unique<tf2_ros::TransformListener>(*tf_buffer_);

    // Set up the input processor
    input_processor_ = std::make_shared<rodoa_input_processor>(tf_buffer_, grid_resolution);
    input_processor_->load_site_map(site_map_path);

    // Set up the thread pool
    thread_pool_ = std::make_unique<rodoa_thread_pool>(input_processor_);
    thread_pool_->start();

    //====================================================================================================================

    timer_ = nh_.createTimer(ros::Duration(1.0), &PointCloudNode::timerCallback, this);

  }

  void timerCallback(const ros::TimerEvent& event){
    ROS_INFO("Timer callback triggered");

    auto tmp_pc = sensor_msgs::PointCloud2();
    cloud_pub_.publish(tmp_pc);

    grid_pub_.publish(*input_processor_->get_grid_map()); // <-- This is the only line of code needed to get the grid map

  }

  void cloudCallback(const sensor_msgs::PointCloud2ConstPtr cloud_msg) {
    
    // Enqueue the input cloud
    thread_pool_->enqueue(*cloud_msg); // <-- This is the only line of code needed to process the input cloud

  }

private:
  ros::NodeHandle nh_;
  ros::Subscriber cloud_sub_;
  ros::Publisher cloud_pub_;
  ros::Publisher grid_pub_;
  std::unique_ptr<tf2_ros::TransformListener> tf_listener_;
  std::shared_ptr<tf2_ros::Buffer> tf_buffer_;
  std::shared_ptr<rodoa_input_processor> input_processor_;
  std::unique_ptr<rodoa_thread_pool> thread_pool_;
  ros::Timer timer_;

};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "point_cloud_node");
  PointCloudNode node("rodoa_site_map.ply", 0.5);
  ros::spin();
  return 0;
}